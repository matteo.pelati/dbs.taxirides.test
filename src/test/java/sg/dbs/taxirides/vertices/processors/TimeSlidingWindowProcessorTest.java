package sg.dbs.taxirides.vertices.processors;

import com.google.common.collect.ImmutableList;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.Outbox;
import com.hazelcast.jet.Processor;
import com.hazelcast.jet.test.TestSupport;
import com.hazelcast.logging.ILogger;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import sg.dbs.taxirides.HazelcastTestUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;


public class TimeSlidingWindowProcessorTest {

    @AllArgsConstructor @Data
    static class WithDateTimeImpl implements WithDateTime {

        @Getter @Setter Date dateTime;

        public WithDateTimeImpl(String s) throws ParseException {
            this.dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(s);
        }
    }

    @Test
    public void testSingleItemRemoval() throws ParseException {


        List<WithDateTime> input = ImmutableList.of(
            new WithDateTimeImpl("2016-01-01 10:00:00"),
            new WithDateTimeImpl("2016-01-01 10:10:00"),
            new WithDateTimeImpl("2016-01-01 10:30:00"),
            new WithDateTimeImpl("2016-01-01 10:31:00")
        );

        List<TimeSlidingWindowProcessor.WindowOperation> output = ImmutableList.of(
            new TimeSlidingWindowProcessor.WindowOperation(
                    ImmutableList.of(),
                    ImmutableList.of(new WithDateTimeImpl("2016-01-01 10:00:00"))
            ) ,
            new TimeSlidingWindowProcessor.WindowOperation(
                    ImmutableList.of(),
                    ImmutableList.of(new WithDateTimeImpl("2016-01-01 10:10:00"))
            ),
            new TimeSlidingWindowProcessor.WindowOperation(
                    ImmutableList.of(),
                    ImmutableList.of(new WithDateTimeImpl("2016-01-01 10:30:00"))
            ),
            new TimeSlidingWindowProcessor.WindowOperation(
                ImmutableList.of(new WithDateTimeImpl("2016-01-01 10:00:00")),
                ImmutableList.of(new WithDateTimeImpl("2016-01-01 10:31:00"))
            )
        );

        TimeSlidingWindowProcessor p = new TimeSlidingWindowProcessor(30, TimeUnit.MINUTES);

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));
        assert(output.equals(outbox.getLs()));


    }

    @Test
    public void testMultipleItemsRemoval() throws ParseException {


        List<WithDateTime> input = ImmutableList.of(
                new WithDateTimeImpl("2016-01-01 10:00:00"),
                new WithDateTimeImpl("2016-01-01 10:10:00"),
                new WithDateTimeImpl("2016-01-01 10:30:00"),
                new WithDateTimeImpl("2016-01-01 11:01:00")
        );

        List<TimeSlidingWindowProcessor.WindowOperation> output = ImmutableList.of(
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(new WithDateTimeImpl("2016-01-01 10:00:00"))
                ) ,
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(new WithDateTimeImpl("2016-01-01 10:10:00"))
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(new WithDateTimeImpl("2016-01-01 10:30:00"))
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(
                                new WithDateTimeImpl("2016-01-01 10:00:00"),
                                new WithDateTimeImpl("2016-01-01 10:10:00"),
                                new WithDateTimeImpl("2016-01-01 10:30:00")
                        ),
                        ImmutableList.of(new WithDateTimeImpl("2016-01-01 11:01:00"))
                )
        );

        TimeSlidingWindowProcessor p = new TimeSlidingWindowProcessor(30, TimeUnit.MINUTES);

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));
        assert(output.equals(outbox.getLs()));


    }

}