package sg.dbs.taxirides.vertices.processors;

import com.google.common.collect.ImmutableList;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import sg.dbs.taxirides.HazelcastTestUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;


public class LeaderboardProcessorTest {


    @Data
    static class WithLocationPathImpl implements WithLocationsPath {

        @Getter @Setter LocationPath locationPath;

        public WithLocationPathImpl(double fromX, double fromY, double toX, double toY) {
            locationPath = new LocationPath(fromX, fromY, toX, toY);
        }

    }


    @Test
    public void testSingleItemInsertion() {

        List<TimeSlidingWindowProcessor.WindowOperation> input = ImmutableList.of(
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                )
        );

        List<LeaderboardProcessor.Leaderboard> output = ImmutableList.of(
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,2, 2)
                        )
                )
        );

        LeaderboardProcessor p = new LeaderboardProcessor(3);

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));
        assert(output.equals(outbox.getLs()));


    }


    @Test
    public void testIncrementalRanking() {

        List<TimeSlidingWindowProcessor.WindowOperation> input = ImmutableList.of(
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                )
        );

        List<LeaderboardProcessor.Leaderboard> output = ImmutableList.of(
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,2, 2)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2),
                                new WithLocationsPath.LocationPath(1, 1,3, 3)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,3, 3)
                        )
                )
        );

        LeaderboardProcessor p = new LeaderboardProcessor(3);

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));
        assert(output.equals(outbox.getLs()));


    }


    @Test
    public void testSwitchRanking() {

        List<TimeSlidingWindowProcessor.WindowOperation> input = ImmutableList.of(
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                )
        );

        List<LeaderboardProcessor.Leaderboard> output = ImmutableList.of(
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,2, 2)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2),
                                new WithLocationsPath.LocationPath(1, 1,3, 3)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,3, 3)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,3, 3),
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,3, 3)
                        )
                )
        );

        LeaderboardProcessor p = new LeaderboardProcessor(3);

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));
        assert(output.equals(outbox.getLs()));


    }

    @Test
    public void testSwitchRankingWithRemoval() {

        List<TimeSlidingWindowProcessor.WindowOperation> input = ImmutableList.of(
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                )
        );

        List<LeaderboardProcessor.Leaderboard> output = ImmutableList.of(
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,2, 2)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2),
                                new WithLocationsPath.LocationPath(1, 1,3, 3)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,3, 3)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,3, 3)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,3, 3)
                        )
                )
        );

        LeaderboardProcessor p = new LeaderboardProcessor(3);

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));
        assert(output.equals(outbox.getLs()));


    }

    @Test
    public void testMaxTop() {

        List<TimeSlidingWindowProcessor.WindowOperation> input = ImmutableList.of(
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,4,4)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,4,4)
                        )
                )

        );

        List<LeaderboardProcessor.Leaderboard> output = ImmutableList.of(
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,2, 2)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2),
                                new WithLocationsPath.LocationPath(1, 1,3, 3)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,3, 3)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2),
                                new WithLocationsPath.LocationPath(1, 1,4, 4)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,4, 4)
                        )
                )
        );

        LeaderboardProcessor p = new LeaderboardProcessor(2);

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));
        assert(output.equals(outbox.getLs()));


    }

    @Test
    public void testRemoveNotInLeaderboard() {

        List<TimeSlidingWindowProcessor.WindowOperation> input = ImmutableList.of(
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,3,3)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,2,2)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,4,4)
                        )
                ),
                new TimeSlidingWindowProcessor.WindowOperation(
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,4,4)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1,1,5,5)
                        )
                )

        );

        List<LeaderboardProcessor.Leaderboard> output = ImmutableList.of(
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,2, 2)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2),
                                new WithLocationsPath.LocationPath(1, 1,3, 3)
                        ),
                        ImmutableList.of(
                                new WithLocationPathImpl(1, 1,3, 3)
                        )
                )
        );

        LeaderboardProcessor p = new LeaderboardProcessor(2);

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));
        assert(output.equals(outbox.getLs()));


    }


}