package sg.dbs.taxirides.vertices.processors;

import com.google.common.collect.ImmutableList;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.junit.Test;
import sg.dbs.taxirides.HazelcastTestUtils;
import sg.dbs.taxirides.entities.TaxiRide;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class LocationMappingProcessorTest {

    @Test
    public void testLongConversionNextBlock() {

        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;

        List<TaxiRide> input = new ArrayList<>();

        TaxiRide t0 = new TaxiRide();
        t0.setPickupLat(41.913406);
        t0.setPickupLong(-74.836339);
        t0.setDropoffLat(41.913406);
        t0.setDropoffLong(geoCalc.calculateEndingGlobalCoordinates(
                reference, new GlobalCoordinates(41.913406, -74.836339),
                90, 500).getLongitude()
        );
        input.add(t0);

        LocationMappingProcessor p = new LocationMappingProcessor(
                41.913406, -74.836339, 500, 300
        );

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));

        assert(
                ((TaxiRide)outbox.getLs().get(0)).getPickupLat() == 1 &&
                ((TaxiRide)outbox.getLs().get(0)).getPickupLong() == 1 &&
                ((TaxiRide)outbox.getLs().get(0)).getDropoffLat() == 1 &&
                ((TaxiRide)outbox.getLs().get(0)).getDropoffLong() == 2
        );


    }

    @Test
    public void testLatConversionNextBlock() {

        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;

        List<TaxiRide> input = new ArrayList<>();

        TaxiRide t0 = new TaxiRide();
        t0.setPickupLat(41.913406);
        t0.setPickupLong(-74.836339);
        t0.setDropoffLong(-74.836339);
        t0.setDropoffLat(geoCalc.calculateEndingGlobalCoordinates(
                reference, new GlobalCoordinates(41.913406, -74.836339),
                180, 500).getLatitude()
        );
        input.add(t0);

        LocationMappingProcessor p = new LocationMappingProcessor(
                41.913406, -74.836339, 500, 300
        );

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));

        assert(
                ((TaxiRide)outbox.getLs().get(0)).getPickupLat() == 1 &&
                        ((TaxiRide)outbox.getLs().get(0)).getPickupLong() == 1 &&
                        ((TaxiRide)outbox.getLs().get(0)).getDropoffLat() == 2 &&
                        ((TaxiRide)outbox.getLs().get(0)).getDropoffLong() == 1
        );


    }

    @Test
    public void testLongConversionLastBlock() {

        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;

        List<TaxiRide> input = new ArrayList<>();

        TaxiRide t0 = new TaxiRide();
        t0.setPickupLat(41.913406);
        t0.setPickupLong(-74.836339);
        t0.setDropoffLat(41.913406);
        t0.setDropoffLong(geoCalc.calculateEndingGlobalCoordinates(
                reference, new GlobalCoordinates(41.913406, -74.836339),
                90, 500*299).getLongitude()
        );
        input.add(t0);

        LocationMappingProcessor p = new LocationMappingProcessor(
                41.913406, -74.836339, 500, 300
        );

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));

        assert(
                ((TaxiRide)outbox.getLs().get(0)).getPickupLat() == 1 &&
                        ((TaxiRide)outbox.getLs().get(0)).getPickupLong() == 1 &&
                        ((TaxiRide)outbox.getLs().get(0)).getDropoffLat() == 1 &&
                        ((TaxiRide)outbox.getLs().get(0)).getDropoffLong() == 300
        );


    }

    @Test
    public void testLatConversionLastBlock() {

        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;

        List<TaxiRide> input = new ArrayList<>();

        TaxiRide t0 = new TaxiRide();
        t0.setPickupLat(41.913406);
        t0.setPickupLong(-74.836339);
        t0.setDropoffLong(-74.836339);
        t0.setDropoffLat(geoCalc.calculateEndingGlobalCoordinates(
                reference, new GlobalCoordinates(41.913406, -74.836339),
                180, 500*299).getLatitude()
        );
        input.add(t0);

        LocationMappingProcessor p = new LocationMappingProcessor(
                41.913406, -74.836339, 500, 300
        );

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));

        assert(
                ((TaxiRide)outbox.getLs().get(0)).getPickupLat() == 1 &&
                        ((TaxiRide)outbox.getLs().get(0)).getPickupLong() == 1 &&
                        ((TaxiRide)outbox.getLs().get(0)).getDropoffLat() == 300 &&
                        ((TaxiRide)outbox.getLs().get(0)).getDropoffLong() == 1
        );


    }

    @Test
    public void testLongConversionOutBlock() {

        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;

        List<TaxiRide> input = new ArrayList<>();

        TaxiRide t0 = new TaxiRide();
        t0.setPickupLat(41.913406);
        t0.setPickupLong(-74.836339);
        t0.setDropoffLat(41.913406);
        t0.setDropoffLong(geoCalc.calculateEndingGlobalCoordinates(
                reference, new GlobalCoordinates(41.913406, -74.836339),
                90, 500*300).getLongitude()
        );
        input.add(t0);

        LocationMappingProcessor p = new LocationMappingProcessor(
                41.913406, -74.836339, 500, 300
        );

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));

        assert(outbox.getLs().isEmpty());


    }

    @Test
    public void testLatConversionOutBlock() {

        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;

        List<TaxiRide> input = new ArrayList<>();

        TaxiRide t0 = new TaxiRide();
        t0.setPickupLat(41.913406);
        t0.setPickupLong(-74.836339);
        t0.setDropoffLong(-74.836339);
        t0.setDropoffLat(geoCalc.calculateEndingGlobalCoordinates(
                reference, new GlobalCoordinates(41.913406, -74.836339),
                180, 500*300).getLatitude()
        );
        input.add(t0);

        LocationMappingProcessor p = new LocationMappingProcessor(
                41.913406, -74.836339, 500, 300
        );

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));

        assert(outbox.getLs().isEmpty());


    }

    @Test
    public void testLongConversionOutBlockLeft() {

        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;

        List<TaxiRide> input = new ArrayList<>();

        TaxiRide t0 = new TaxiRide();
        t0.setPickupLat(41.913406);
        t0.setPickupLong(-74.836339);
        t0.setDropoffLat(41.913406);
        t0.setDropoffLong(geoCalc.calculateEndingGlobalCoordinates(
                reference, new GlobalCoordinates(41.913406, -74.836339),
                270, 500).getLongitude()
        );
        input.add(t0);

        LocationMappingProcessor p = new LocationMappingProcessor(
                41.913406, -74.836339, 500, 300
        );

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));

        assert(outbox.getLs().isEmpty());


    }

    @Test
    public void testLatConversionOutBlockTop() {

        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;

        List<TaxiRide> input = new ArrayList<>();

        TaxiRide t0 = new TaxiRide();
        t0.setPickupLat(41.913406);
        t0.setPickupLong(-74.836339);
        t0.setDropoffLong(-74.836339);
        t0.setDropoffLat(geoCalc.calculateEndingGlobalCoordinates(
                reference, new GlobalCoordinates(41.913406, -74.836339),
                0, 500).getLatitude()
        );
        input.add(t0);

        LocationMappingProcessor p = new LocationMappingProcessor(
                41.913406, -74.836339, 500, 300
        );

        HazelcastTestUtils.SingleBucketMockOutbox outbox = new HazelcastTestUtils.SingleBucketMockOutbox();
        p.init(outbox, new HazelcastTestUtils.MockContext());
        p.process(0, new HazelcastTestUtils.MockInbox(input));

        assert(outbox.getLs().isEmpty());


    }

}