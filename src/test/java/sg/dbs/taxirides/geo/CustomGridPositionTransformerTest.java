package sg.dbs.taxirides.geo;

import org.gavaghan.geodesy.GlobalCoordinates;
import org.junit.Test;


public class CustomGridPositionTransformerTest {


    @Test(expected = IllegalArgumentException.class)
    public void testNorthWestOutOfBounds() {

        GridPositionTransformer t = new GridPositionTransformer(
                new GlobalCoordinates(41.474937d, -74.913585d), 500, 300
        );

        PositionTransformer.Location r = t.transform(new GlobalCoordinates(41.733000, -75.140981));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testSouthEastOutOfBounds() {

        GridPositionTransformer t = new GridPositionTransformer(
                new GlobalCoordinates(41.474937d, -74.913585d), 500, 300
        );

        PositionTransformer.Location r = t.transform(new GlobalCoordinates(40.863613, -72.711007));

    }

    @Test
    public void testInBoundsBoundsStart() {

        GridPositionTransformer t = new GridPositionTransformer(
                new GlobalCoordinates(41.474937d, -74.913585d), 500, 300
        );

        PositionTransformer.Location r = t.transform(new GlobalCoordinates(41.474937d, -74.913585d));
        assert (r.getX() == 1 && r.getY() == 1);

    }

    @Test
    public void testInBoundsBoundsEnd() {

        GridPositionTransformer t = new GridPositionTransformer(
                new GlobalCoordinates(41.474937d, -74.913585d), 500, 300
        );

        PositionTransformer.Location r = t.transform(new GlobalCoordinates(40.127992, -73.1227090));
        assert (r.getX() == 300 && r.getY() == 300);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testOtherEmisphereOutOfBounds() {

        GridPositionTransformer t = new GridPositionTransformer(
                new GlobalCoordinates(41.474937d, -74.913585d), 500, 300
        );

        PositionTransformer.Location r = t.transform(new GlobalCoordinates(-23.230466, 120.026440));

    }

    @Test
    public void testInBoundsBoundsEast() {

        GridPositionTransformer t = new GridPositionTransformer(
                new GlobalCoordinates(41.474937d, -74.913585d), 500, 300
        );

        PositionTransformer.Location r = t.transform(new GlobalCoordinates(41.209805, -73.981714));
        assert (r.getX() > r.getY());

    }


    @Test
    public void testInBoundsBoundsSouth() {

        GridPositionTransformer t = new GridPositionTransformer(
                new GlobalCoordinates(41.474937d, -74.913585d), 500, 300
        );

        PositionTransformer.Location r = t.transform(new GlobalCoordinates(40.631491, -74.713287));
        assert (r.getX() < r.getY());

    }



}