package sg.dbs.taxirides;

import com.google.common.collect.ImmutableList;
import com.hazelcast.jet.*;
import com.hazelcast.jet.config.JetConfig;
import com.hazelcast.jet.processor.Sinks;
import com.hazelcast.jet.processor.Sources;
import com.hazelcast.jet.stream.IStreamList;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sg.dbs.taxirides.entities.TaxiRide;
import sg.dbs.taxirides.vertices.processors.*;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;



@RunWith(Parameterized.class)
public class E2ETest {


    @Data
    static class T implements WithLocationsPath, WithDateTime, Serializable {

        @Getter @Setter LocationPath locationPath;
        @Getter @Setter Date dateTime;

        public T(String s, double fromX, double fromY, double toX, double toY) throws ParseException {
            locationPath = new LocationPath(fromX, fromY, toX, toY);
            this.dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(s);
        }

    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { 1 } //, { 5 }
        });
    }


    int threads;

    public E2ETest(int threads) {
        this.threads = threads;
    }

    @Test
    public void e2eTest() throws Exception {


        DAG dag = new DAG();


        dag.newVertex("input", Sources.readList("input")).localParallelism(1);

        TimeSlidingWindowProcessorProvider window = new TimeSlidingWindowProcessorProvider();
        window.setId("window");
        window.setWindowSize(30);
        window.setTimeUnit(TimeUnit.MINUTES);
        dag.vertex(window.build());

        LeaderboardProcessorProvider ldb = new LeaderboardProcessorProvider();
        ldb.setId("ldb");
        ldb.setTopCount(2);
        dag.vertex(ldb.build());

        dag.newVertex("output", Sinks.writeList("output")).localParallelism(1);

        dag.edge(Edge.between(dag.getVertex("input"), dag.getVertex("window")));
        dag.edge(Edge.between(dag.getVertex("window"), dag.getVertex("ldb")));
        dag.edge(Edge.between(dag.getVertex("ldb"), dag.getVertex("output")));

        JetConfig jetConfig = new JetConfig();
        jetConfig.getInstanceConfig().setCooperativeThreadCount(threads);
        jetConfig.getHazelcastConfig().setProperty("hazelcast.discovery.enabled", "false");
        jetConfig.getHazelcastConfig().setInstanceName(UUID.randomUUID().toString());

        JetInstance jet = Jet.newJetInstance(jetConfig);


        IStreamList<T> input = jet.getList("input");
        input.addAll(ImmutableList.of(
            new T("2016-01-01 10:00:00", 1, 1, 2, 2),
            new T("2016-01-01 10:10:00", 1, 1, 2, 2),
            new T("2016-01-01 10:20:00", 1, 1, 2, 2),
            new T("2016-01-01 10:30:00", 1, 1, 2, 2),
            new T("2016-01-01 11:40:00", 1, 1, 2, 2),
            new T("2016-01-01 11:45:00", 1, 1, 3, 3),
            new T("2016-01-01 11:50:00", 1, 1, 3, 3),
            new T("2016-01-01 12:50:00", 1, 1, 4, 4),
            new T("2016-01-01 12:50:00", 1, 1, 4, 4),
            new T("2016-01-01 12:50:00", 1, 1, 4, 4),
            new T("2016-01-01 12:50:00", 1, 1, 4, 4),
            new T("2016-01-01 12:51:00", 1, 1, 2, 2),
            new T("2016-01-01 12:51:00", 1, 1, 2, 2),
            new T("2016-01-01 12:51:00", 1, 1, 2, 2),
            new T("2016-01-01 12:52:00", 1, 1, 6, 6),
            new T("2016-01-01 13:21:00", 1, 1, 6, 6),
            new T("2016-01-01 13:22:00", 1, 1, 6, 6),
            new T("2016-01-01 14:22:00", 1, 1, 6, 6),
            new T("2016-01-01 15:22:00", 1, 1, 7, 7)
        ));

        jet.newJob(dag).execute().get();

        List<LeaderboardProcessor.Leaderboard> expectedOutput = ImmutableList.of(
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new T("2016-01-01 10:00:00", 1, 1,2, 2)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2),
                                new WithLocationsPath.LocationPath(1, 1,3, 3)
                        ),
                        ImmutableList.of(
                                new T("2016-01-01 11:45:00", 1, 1,3, 3)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,3, 3),
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new T("2016-01-01 11:50:00", 1, 1,3, 3)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,4, 4)
                        ),
                        ImmutableList.of(
                                new T("2016-01-01 12:50:00", 1, 1,4, 4)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,4, 4),
                                new WithLocationsPath.LocationPath(1, 1,2, 2)
                        ),
                        ImmutableList.of(
                                new T("2016-01-01 12:51:00", 1, 1,2, 2)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,2, 2),
                                new WithLocationsPath.LocationPath(1, 1,6, 6)
                        ),
                        ImmutableList.of(
                                new T("2016-01-01 13:21:00", 1, 1,6, 6)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,6, 6)
                        ),
                        ImmutableList.of(
                                new T("2016-01-01 13:22:00", 1, 1,6, 6)
                        )
                ),
                new LeaderboardProcessor.Leaderboard(
                        ImmutableList.of(
                                new WithLocationsPath.LocationPath(1, 1,7, 7)
                        ),
                        ImmutableList.of(
                                new T("2016-01-01 15:22:00", 1, 1,7, 7)
                        )
                )
        );


        IStreamList<T> output = jet.getList("output");
        assert (new ArrayList<T>(output).equals(expectedOutput));
        jet.shutdown();


    }

}
