package sg.dbs.taxirides;

import com.google.common.collect.ImmutableList;
import com.hazelcast.jet.*;
import com.hazelcast.jet.config.JetConfig;
import com.hazelcast.jet.processor.Sinks;
import com.hazelcast.jet.processor.Sources;
import com.hazelcast.jet.stream.IStreamList;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.junit.Ignore;
import org.junit.Test;
import sg.dbs.taxirides.config.AbstractVertexProvider;
import sg.dbs.taxirides.vertices.processors.*;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class LoadTest {


    @Data
    static class T implements WithLocationsPath, WithDateTime, Serializable {

        @Getter @Setter LocationPath locationPath;
        @Getter @Setter Date dateTime;

        public T(long ts, double fromX, double fromY, double toX, double toY) {
            locationPath = new LocationPath(fromX, fromY, toX, toY);
            this.dateTime = new Date(ts);
        }

    }

    static class GenerateT extends AbstractProcessor implements Serializable {

        final long total = 30000000;
        AtomicLong count;
        long startTime;
        boolean done = false;
        private final Traverser<T> traverser = () -> {
            if (count.get() % 100000 == 0)
                System.out.println(count.get());
            if (done)
                return null;
            if (count.incrementAndGet() == total) {
                done = true;
                return new T(startTime + total * 100 + 1000*60*60, 1, 1, count.get(), count.get());
            }
            return new T(startTime + count.get()*100, 1, 1, count.get(), count.get());
        };

        public GenerateT() {
            try {
                startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2016-01-01 00:00:00").getTime();
                count = new AtomicLong();
            }
            catch (ParseException e) {
                throw new IllegalArgumentException(e);
            }
        }


        @Override
        public boolean complete() {
            return emitFromTraverser(traverser);
        }
    }

    static class GenerateTProvider extends AbstractVertexProvider implements Serializable {

        @Override
        public Vertex build() {
                return new Vertex(id, new ProcessorSupplier() {
                    @Override
                    public Collection<? extends Processor> get(int i) {
                            return IntStream.range(0, i).mapToObj(x -> new GenerateT()).collect(Collectors.toList());
                    }
                }).localParallelism(parallelism);
        }

    }


    @Test @Ignore
    public void e2eTest() throws Exception {


        DAG dag = new DAG();


        GenerateTProvider source = new GenerateTProvider();
        source.setId("input");
        dag.vertex(source.build());

        TimeSlidingWindowProcessorProvider window = new TimeSlidingWindowProcessorProvider();
        window.setId("window");
        window.setWindowSize(30);
        window.setTimeUnit(TimeUnit.MINUTES);
        dag.vertex(window.build());

        LeaderboardProcessorProvider ldb = new LeaderboardProcessorProvider();
        ldb.setId("ldb");
        ldb.setTopCount(10);
        dag.vertex(ldb.build());

        dag.edge(Edge.between(dag.getVertex("input"), dag.getVertex("window")));
        dag.edge(Edge.between(dag.getVertex("window"), dag.getVertex("ldb")));

        JetConfig jetConfig = new JetConfig();
        jetConfig.getInstanceConfig().setCooperativeThreadCount(10);
        jetConfig.getHazelcastConfig().setProperty("hazelcast.discovery.enabled", "false");
        jetConfig.getHazelcastConfig().setInstanceName(UUID.randomUUID().toString());

        JetInstance jet = Jet.newJetInstance(jetConfig);

        jet.newJob(dag).execute().get();
        jet.shutdown();

    }

    @Test @Ignore
    public void createTestFile() throws Exception {

        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;
        SimpleDateFormat ds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Random r = new Random();

        FileOutputStream os = new FileOutputStream("testfile.csv");
        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(os));

        long startDate = new Date().getTime();
        for (int ctr=0;ctr<20000000;ctr++) {

            GlobalCoordinates start = new GlobalCoordinates(41.474937d, -74.913585);

            GlobalCoordinates end = geoCalc.calculateEndingGlobalCoordinates(
                    reference, new GlobalCoordinates(41.474937d, -74.913585),
                    180, r.nextInt(120000)
            );

            String line = String.format(
                    "N, N, %s, %s, 0, 0, %f, %f, %f, %f, 0, 0, 0, 0, 0, 0, 0\n",
                    ds.format(new Date(startDate)), ds.format(new Date(startDate)),
                    start.getLongitude(), start.getLatitude(), end.getLongitude(), end.getLatitude()
            );

            w.write(line);
            startDate += 200;

        }

        w.flush();
        os.flush();
        w.close();
        os.close();

    }

}
