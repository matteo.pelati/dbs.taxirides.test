package sg.dbs.taxirides;

import com.hazelcast.jet.Inbox;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.Outbox;
import com.hazelcast.jet.Processor;
import com.hazelcast.logging.ILogger;
import lombok.Getter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;


public class HazelcastTestUtils {

    public static class SingleBucketMockOutbox implements Outbox {

        @Getter List ls = new ArrayList();

        @Override
        public int bucketCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean offer(int i, Object o) {
            return ls.add(o);
        }

        @Override
        public boolean offer(int[] ints, Object o) {
            return ls.add(o);
        }

        @Override
        public boolean offer(Object o) {
            return ls.add(o);
        }
    }

    public static class MockInbox implements Inbox {

        LinkedList items;

        public MockInbox(List items) {
            this.items = new LinkedList(items);
        }

        @Override
        public boolean isEmpty() {
            return items.isEmpty();
        }

        @Override
        public Object peek() {
            return items.peek();
        }

        @Override
        public Object poll() {
            return items.poll();
        }

        @Override
        public Object remove() {
            return items.removeFirst();
        }
    }

    public static class MockContext implements Processor.Context {

        @Override
        public JetInstance jetInstance() {
            return null;
        }

        @Override
        public ILogger logger() {
            return null;
        }

        @Override
        public int globalProcessorIndex() {
            return 0;
        }

        @Override
        public String vertexName() {
            return null;
        }

        @Override
        public CompletableFuture<Void> jobFuture() {
            return null;
        }
    }



}
