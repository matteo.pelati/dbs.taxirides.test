package sg.dbs.taxirides.config;

import com.hazelcast.config.DiscoveryStrategyConfig;


public interface ClusterDiscoverer {

    DiscoveryStrategyConfig getDiscoveryStrategy();

}
