package sg.dbs.taxirides.config;

import com.hazelcast.jet.DAG;
import com.hazelcast.jet.Edge;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


public class EdgeProvider implements Serializable {

    @Getter @Setter String from;
    @Getter @Setter String to;

    public Edge build(DAG dag) {

        Edge e = Edge.between(dag.getVertex(from), dag.getVertex(to));
        return e;
    }
}
