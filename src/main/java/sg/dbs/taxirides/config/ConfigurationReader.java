package sg.dbs.taxirides.config;


public interface ConfigurationReader {
    Configuration load(String path) throws Exception;
}
