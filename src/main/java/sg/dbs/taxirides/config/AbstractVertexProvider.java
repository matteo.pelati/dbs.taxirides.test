package sg.dbs.taxirides.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
public abstract class AbstractVertexProvider implements VertexProvider {

    @Getter @Setter protected String id;
    @Getter @Setter protected int parallelism = 1;

}
