package sg.dbs.taxirides.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
public class Configuration {

    @Getter @Setter ClusterDiscoverer discoverer;
    @Getter @Setter String username = "";
    @Getter @Setter String password = "";
    @Getter @Setter int threads = 1;

    @Getter @Setter VertexProvider[] vertices = new VertexProvider[0];
    @Getter @Setter EdgeProvider[] edges = new EdgeProvider[0];

}
