package sg.dbs.taxirides.config;

import com.hazelcast.jet.Vertex;

import java.io.Serializable;


public interface VertexProvider extends Serializable {

    String getId();
    void setId(String id);
    Vertex build();

}
