package sg.dbs.taxirides.config;

import com.hazelcast.config.DiscoveryStrategyConfig;
import com.hazelcast.zookeeper.ZookeeperDiscoveryProperties;
import com.hazelcast.zookeeper.ZookeeperDiscoveryStrategyFactory;
import lombok.Getter;
import lombok.Setter;


public class ZkClusterDiscoverer implements ClusterDiscoverer {

    @Getter @Setter String zkQuorum;
    @Getter @Setter String zkPath = "/dbs/taxirides/hazelcast";
    @Getter @Setter String clusterId = "default";

    @Override
    public DiscoveryStrategyConfig getDiscoveryStrategy() {

        DiscoveryStrategyConfig discoveryStrategyConfig = new DiscoveryStrategyConfig(new ZookeeperDiscoveryStrategyFactory());
        discoveryStrategyConfig.addProperty(ZookeeperDiscoveryProperties.ZOOKEEPER_URL.key(), zkQuorum);
        discoveryStrategyConfig.addProperty(ZookeeperDiscoveryProperties.ZOOKEEPER_PATH.key(), clusterId);
        discoveryStrategyConfig.addProperty(ZookeeperDiscoveryProperties.GROUP.key(), clusterId);
        return discoveryStrategyConfig;

    }
}
