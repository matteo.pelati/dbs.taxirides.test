package sg.dbs.taxirides;

import com.hazelcast.jet.DAG;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetConfig;
import sg.dbs.taxirides.config.Configuration;
import sg.dbs.taxirides.config.YamlConfigurationReader;

import java.io.File;
import java.util.Arrays;
import java.util.UUID;


public class LocalRunner {

    static DAG buildDag(Configuration config) {

        DAG dag = new DAG();

        Arrays.stream(config.getVertices()).forEach(v -> dag.vertex(v.build()));
        Arrays.stream(config.getEdges()).forEach(e -> dag.edge(e.build(dag)));
        return dag;
    }

    public static void main(String[] args) throws Exception {

        if (args.length < 1) {
            System.out.println("Missing Arguments. Expected: [configuration file]");
            System.exit(-1);
        }

        File f = new File(args[0]);
        if (!f.exists())
            throw new IllegalArgumentException("Configuration file does not exist");

        YamlConfigurationReader reader = new YamlConfigurationReader();
        Configuration conf = reader.load("file:" + f.getAbsolutePath());

        DAG dag = buildDag(conf);


        JetConfig jetConfig = new JetConfig();
        jetConfig.getInstanceConfig().setCooperativeThreadCount(conf.getThreads());
        jetConfig.getHazelcastConfig().setProperty("hazelcast.discovery.enabled", "false");
        jetConfig.getHazelcastConfig().setInstanceName(UUID.randomUUID().toString());

        JetInstance jet = Jet.newJetInstance(jetConfig);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> jet.shutdown()));

        jet.newJob(dag).execute().get();

        while (true) {
            Thread.sleep(1000);
        }


    }

}
