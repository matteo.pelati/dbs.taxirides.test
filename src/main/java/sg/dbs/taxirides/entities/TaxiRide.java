package sg.dbs.taxirides.entities;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.collect.ImmutableList;
import lombok.Data;
import lombok.NoArgsConstructor;
import sg.dbs.taxirides.geo.PositionTransformer;
import sg.dbs.taxirides.vertices.processors.WithLocationsPath;
import sg.dbs.taxirides.vertices.processors.WithDateTime;
import sg.dbs.taxirides.vertices.processors.WithTimestamp;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Data @JsonPropertyOrder({
        "medallion", "hackLicense", "pickupTime", "dropoffTime", "tripDuration", "tripDistance",
        "pickupLong", "pickupLat", "dropoffLong", "dropoffLat","paymentType", "fare", "surcharge",
        "tax", "tip", "tolls", "total"
})
@JsonAutoDetect(
        fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE
)
@NoArgsConstructor
public class TaxiRide implements Serializable, WithDateTime, WithLocationsPath, WithTimestamp {

    String medallion;
    String hackLicense;
    Date pickupTime;
    Date dropoffTime;
    int tripDuration;
    float tripDistance;
    double pickupLong;
    double pickupLat;
    double dropoffLong;
    double dropoffLat;
    String paymentType;
    float fare;
    float surcharge;
    float tax;
    float tip;
    float tolls;
    float total;
    long timestamp;

    @Override
    public Date getDateTime() {
        return dropoffTime;
    }

    @Override
    public void setDateTime(Date d) {
        dropoffTime = d;
    }

    @Override
    public LocationPath getLocationPath() {
        return new LocationPath(
                pickupLong, pickupLat, dropoffLong, dropoffLat
        );
    }

    @Override
    public void setLocationPath(LocationPath path) {
        this.pickupLong = path.getFromX();
        this.pickupLat = path.getFromY();
        this.dropoffLong = path.getToX();
        this.dropoffLat = path.getToY();
    }

}
