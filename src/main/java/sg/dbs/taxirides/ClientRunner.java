package sg.dbs.taxirides;

import com.google.common.collect.ImmutableList;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.jet.DAG;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import sg.dbs.taxirides.config.Configuration;
import sg.dbs.taxirides.config.YamlConfigurationReader;


import java.io.File;
import java.util.Arrays;


public class ClientRunner {


    static DAG buildDag(Configuration config) {

        DAG dag = new DAG();

        Arrays.stream(config.getVertices()).forEach(v -> dag.vertex(v.build()));
        Arrays.stream(config.getEdges()).forEach(e -> dag.edge(e.build(dag)));
        return dag;
    }

    public static void main(String[] args) throws Exception {

        if (args.length < 1) {
            System.out.println("Missing Arguments. Expected: [configuration file]");
            System.exit(-1);
        }

        File f = new File(args[0]);
        if (!f.exists())
            throw new IllegalArgumentException("Configuration file does not exist");

        YamlConfigurationReader reader = new YamlConfigurationReader();
        Configuration conf = reader.load("file:" + f.getAbsolutePath());

        DAG dag = buildDag(conf);

        ClientConfig jetConfig = new ClientConfig();
        jetConfig.getNetworkConfig().getDiscoveryConfig().setDiscoveryStrategyConfigs(
                ImmutableList.of(conf.getDiscoverer().getDiscoveryStrategy())
        );
        jetConfig.getGroupConfig().setName(conf.getUsername());
        jetConfig.getGroupConfig().setPassword(conf.getPassword());

        JetInstance jetClient = Jet.newJetClient(jetConfig);
        jetClient.newJob(dag).execute().get();

        System.exit(0);


    }

}
