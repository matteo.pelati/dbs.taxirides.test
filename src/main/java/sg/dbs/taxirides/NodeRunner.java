package sg.dbs.taxirides;

import com.google.common.collect.ImmutableList;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetConfig;
import sg.dbs.taxirides.config.Configuration;
import sg.dbs.taxirides.config.YamlConfigurationReader;

import java.io.File;
import java.util.UUID;


public class NodeRunner {

    public static void main(String[] args) throws Exception {

        if (args.length < 1) {
            System.out.println("Missing Arguments. Expected: [configuration file]");
            System.exit(-1);
        }

        File f = new File(args[0]);
        if (!f.exists())
            throw new IllegalArgumentException("Configuration file does not exist");

        YamlConfigurationReader reader = new YamlConfigurationReader();
        Configuration conf = reader.load("file:" + f.getAbsolutePath());

        JetConfig jetConfig = new JetConfig();
        jetConfig.getHazelcastConfig().getGroupConfig().setName(conf.getUsername());
        jetConfig.getHazelcastConfig().getGroupConfig().setPassword(conf.getPassword());
        jetConfig.getHazelcastConfig().setProperty("hazelcast.discovery.enabled", "true");
        jetConfig.getHazelcastConfig().setInstanceName(UUID.randomUUID().toString());
        jetConfig.getHazelcastConfig().getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
        jetConfig.getHazelcastConfig().getNetworkConfig().getJoin().getDiscoveryConfig().setDiscoveryStrategyConfigs(
                ImmutableList.of(conf.getDiscoverer().getDiscoveryStrategy())
        );

        JetInstance node = Jet.newJetInstance(jetConfig);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> node.shutdown()));

        while (true) {
            Thread.sleep(1000);
        }

    }

}
