package sg.dbs.taxirides.vertices.sources;

import com.hazelcast.jet.Vertex;
import com.hazelcast.jet.processor.Sources;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.FileUtils;
import sg.dbs.taxirides.config.AbstractVertexProvider;

import java.io.File;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Properties;


public class FileStreamSourceProvider extends AbstractVertexProvider implements Serializable {

    @Getter @Setter String directory;
    @Getter @Setter boolean cleanDirectory = true;

    @Override
    public Vertex build() {

        try {

            File fd = new File(directory);
            if (cleanDirectory)
                FileUtils.deleteDirectory(fd);
            if (!fd.exists())
                fd.mkdirs();

            return new Vertex(id, Sources.streamFiles(directory, StandardCharsets.UTF_8, "*")).localParallelism(parallelism);
        }
        catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
