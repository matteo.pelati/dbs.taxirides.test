package sg.dbs.taxirides.vertices.processors;


public interface WithTimestamp {

    long getTimestamp();
    void setTimestamp(long ts);
}
