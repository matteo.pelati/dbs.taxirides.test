package sg.dbs.taxirides.vertices.processors;

import com.google.common.collect.ImmutableList;
import com.hazelcast.jet.AbstractProcessor;
import com.hazelcast.jet.Traversers;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;


public class LeaderboardProcessor extends AbstractProcessor {

    @AllArgsConstructor @Data
    static class LocationPathInfo {
        long id;
        long counter;
    }

    @AllArgsConstructor @Data
    static class LocationPathRank implements Comparable<LocationPathRank> {

        long id;
        long counter;

        @Override
        public int compareTo(LocationPathRank o) {

            int rank = Long.compare(o.counter, counter);
            if (rank > 0 || rank < 0) return rank;
            return Long.compare(id, o.id);

        }
    }

    @AllArgsConstructor @Data
    public static class Leaderboard implements Serializable {
        List<WithLocationsPath.LocationPath> top;
        List added;
    }

    Logger logger = LoggerFactory.getLogger(this.getClass());
    int topCount;

    private final FlatMapper<Object, Object> flatMapper =
            flatMapper(o -> Traversers.traverseIterable(ImmutableList.of(o)));

    AtomicLong idGenerator = new AtomicLong(0);
    Map<WithLocationsPath.LocationPath, LocationPathInfo> locationPathsIndex = new HashMap<>();
    Map<Long, WithLocationsPath.LocationPath> locationPathsInverseIndex = new HashMap<>();
    SortedSet<LocationPathRank> leaderboard = new TreeSet<>();
    long[] prevLeaders = new long[0];

    public LeaderboardProcessor(int topCount) {
        this.topCount = topCount;
    }

    LocationPathInfo getLocationPathInfo(WithLocationsPath.LocationPath path) {

        LocationPathInfo info = locationPathsIndex.get(path);
        if (info == null) {
            long newId = idGenerator.incrementAndGet();
            info = new LocationPathInfo(newId, 0l);
            locationPathsInverseIndex.put(newId, path);
            locationPathsIndex.put(path, info);
        }
        return info;
    }

    long[] getTops(SortedSet<LocationPathRank> paths) {

        long[] res = new long[Math.min(topCount, paths.size())];
        Iterator<LocationPathRank> i = paths.iterator();
        for (int ctr=0;ctr<res.length;ctr++) {
            res[ctr] = i.next().id;
        }
        return res;
    }

    @Override
    protected synchronized boolean tryProcess(int ordinal, Object item) throws Exception {

        TimeSlidingWindowProcessor.WindowOperation op = (TimeSlidingWindowProcessor.WindowOperation) item;

        op.getAdded().stream().forEach(a -> {

            WithLocationsPath.LocationPath added = ((WithLocationsPath)a).getLocationPath();
            LocationPathInfo addedInfo = getLocationPathInfo(added);
            leaderboard.remove(new LocationPathRank(addedInfo.id, addedInfo.counter));
            addedInfo.counter += 1;
            leaderboard.add(new LocationPathRank(addedInfo.id, addedInfo.counter));

        });

        op.getRemoved().stream().forEach(r -> {

            WithLocationsPath.LocationPath removed = ((WithLocationsPath)r).getLocationPath();
            LocationPathInfo removedInfo = getLocationPathInfo(removed);
            leaderboard.remove(new LocationPathRank(removedInfo.id, removedInfo.counter));
            removedInfo.counter -= 1;
            if (removedInfo.counter > 0)
                leaderboard.add(new LocationPathRank(removedInfo.id, removedInfo.counter));
        });

        long[] currLeaders = getTops(leaderboard);
        if (!Arrays.equals(currLeaders, prevLeaders)) {
            flatMapper.tryProcess(
                    new Leaderboard(
                            Arrays.stream(currLeaders).mapToObj(id -> locationPathsInverseIndex.get(id)).collect(Collectors.toList()),
                            op.getAdded()
                    )
            );
            prevLeaders = currLeaders;
        }

        return true;

    }
}
