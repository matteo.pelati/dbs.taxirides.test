package sg.dbs.taxirides.vertices.processors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sg.dbs.taxirides.geo.PositionTransformer;

import java.io.Serializable;
import java.util.List;


public interface WithLocationsPath {

    @Data @NoArgsConstructor @AllArgsConstructor
    class LocationPath implements Serializable {
        double fromX;
        double fromY;
        double toX;
        double toY;
    }


    LocationPath getLocationPath();
    void setLocationPath(LocationPath path);

}
