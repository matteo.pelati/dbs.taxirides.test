package sg.dbs.taxirides.vertices.processors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.common.collect.ImmutableList;
import com.hazelcast.jet.AbstractProcessor;
import com.hazelcast.jet.Traversers;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;



public class Csv2BeanProcessor extends AbstractProcessor {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    CsvMapper mapper = new CsvMapper();
    ObjectReader reader;

    private final FlatMapper<Object, Object> flatMapper =
            flatMapper(o -> Traversers.traverseIterable(ImmutableList.of(o)));

    public Csv2BeanProcessor(Class<?> entity, String dateFormat) {

        CsvSchema schema = mapper.schemaFor(entity);
        DateFormat df = new SimpleDateFormat(dateFormat);
        mapper.setDateFormat(df);
        reader = mapper.reader(schema).forType(entity);
    }

    @Override
    protected boolean tryProcess(int ordinal, Object item) throws Exception {
        try {

            long ts = System.currentTimeMillis();
            Object r = reader.readValue(item.toString());
            if (r instanceof WithTimestamp)
                ((WithTimestamp) r).setTimestamp(ts);
            flatMapper.tryProcess(r);
        }
        catch (JsonProcessingException e) {
           //  logger.warn("Unable to parse line : " + item.toString(), e);
        }
        return true;
    }
}
