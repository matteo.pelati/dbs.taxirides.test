package sg.dbs.taxirides.vertices.processors;

import java.util.Date;


public interface WithDateTime {

    Date getDateTime();
    void setDateTime(Date d);
}
