package sg.dbs.taxirides.vertices.processors;

import com.hazelcast.jet.AbstractProcessor;
import sg.dbs.taxirides.entities.TaxiRide;

import java.text.SimpleDateFormat;
import java.util.stream.Collectors;


public class ConsoleLoggerProcessor extends AbstractProcessor {

    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected boolean tryProcess(int ordinal, Object item) throws Exception {

        LeaderboardProcessor.Leaderboard ld = (LeaderboardProcessor.Leaderboard) item;

        if (ld.top.size() < 10) {
            System.out.println("NULL");
            return true;
        }

        TaxiRide ride = (TaxiRide) ld.added.get(0);

        String blocks = String.join(
                ", ",
                ld.top.stream().map(l -> String.format("%d.%d, %s.%d", (int)l.getFromX(), (int)l.getFromY(), (int)l.getToX(), (int)l.getToY()))
                .collect(Collectors.toList())
        );

        System.out.println(String.format(
                "%s, %s, %s, %d",
                df.format(ride.getPickupTime()), df.format(ride.getDropoffTime()), blocks,
                System.currentTimeMillis() - ride.getTimestamp()
        ));

        return true;
    }
}
