package sg.dbs.taxirides.vertices.processors;

import com.google.common.collect.ImmutableList;
import com.hazelcast.jet.AbstractProcessor;
import com.hazelcast.jet.Traversers;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;



@NoArgsConstructor
public class TimeSlidingWindowProcessor extends AbstractProcessor {

    @AllArgsConstructor @NoArgsConstructor @Data
    public static class WindowOperation implements Serializable {
        List removed;
        List added;
    }


    private final FlatMapper<WindowOperation, Object> flatMapper =
            flatMapper(op -> Traversers.traverseIterable(ImmutableList.of(op)));

    long duration;
    LinkedList<WithDateTime> window = new LinkedList<>();

    public TimeSlidingWindowProcessor(long duration, TimeUnit unit) {
        this.duration = TimeUnit.MILLISECONDS.convert(duration, unit);
    }

    List<WithDateTime> add(WithDateTime item) {

        List removed = new ArrayList();
        window.add(item);
        long lowerBound = item.getDateTime().getTime() - duration;
        while (window.peekFirst().getDateTime().getTime() < lowerBound) {
            removed.add(window.removeFirst());
        }
        return removed;
    }

    @Override
    protected synchronized boolean tryProcess(int ordinal, Object item) throws Exception {

        if (window.size() > 0 && ((WithDateTime)item).getDateTime().getTime() < window.getLast().getDateTime().getTime()) {
//            System.out.println("ivalid date");
            return true;
        }

        List removed = add((WithDateTime) item);
        flatMapper.tryProcess(new WindowOperation(removed, ImmutableList.of(item)));
        return true;
    }
}

