package sg.dbs.taxirides.vertices.processors;

import com.hazelcast.jet.Processor;
import com.hazelcast.jet.ProcessorSupplier;
import com.hazelcast.jet.Vertex;
import lombok.Getter;
import lombok.Setter;
import sg.dbs.taxirides.config.AbstractVertexProvider;

import java.io.Serializable;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class Csv2BeanProcessorProvider extends AbstractVertexProvider implements Serializable {

    @Getter @Setter Class beanType;
    @Getter @Setter String dateFormat;

    @Override
    public Vertex build() {
        return new Vertex(id, new ProcessorSupplier() {
            @Override
            public Collection<? extends Processor> get(int i) {
                return IntStream.range(0, i).mapToObj(x -> new Csv2BeanProcessor(
                        beanType, dateFormat
                )).collect(Collectors.toList());
            }
        }).localParallelism(parallelism);
    }
}
