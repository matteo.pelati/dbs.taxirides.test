package sg.dbs.taxirides.vertices.processors;

import com.google.common.collect.ImmutableList;
import com.hazelcast.jet.AbstractProcessor;
import com.hazelcast.jet.Traversers;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sg.dbs.taxirides.geo.GridPositionTransformer;
import sg.dbs.taxirides.geo.PositionTransformer;

import java.util.ArrayList;
import java.util.List;


public class LocationMappingProcessor extends AbstractProcessor {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    PositionTransformer transformer;

    private final FlatMapper<Object, Object> flatMapper =
            flatMapper(o -> Traversers.traverseIterable(ImmutableList.of(o)));

    public LocationMappingProcessor(double startLat, double startLong, int blockSize, int blockCount) {

        transformer = new GridPositionTransformer(
          new GlobalCoordinates(startLat, startLong), blockSize, blockCount
        );

    }

    @Override
    protected boolean tryProcess(int ordinal, Object item) throws Exception {

        try {

            WithLocationsPath.LocationPath path = ((WithLocationsPath) item).getLocationPath();

            PositionTransformer.Location fromBlock = transformer.transform(
                    new GlobalCoordinates(path.getFromY(), path.getFromX())
            );
            PositionTransformer.Location toBlock = transformer.transform(
                    new GlobalCoordinates(path.getToY(), path.getToX())
            );

            ((WithLocationsPath) item).setLocationPath(new WithLocationsPath.LocationPath(
                    fromBlock.getX(), fromBlock.getY(), toBlock.getX(), toBlock.getY()
            ));

            flatMapper.tryProcess(item);

        }
        catch (IllegalArgumentException e) {
          //  logger.warn(e.getMessage() + ": " + item.toString());
        }

        return true;
    }
}
