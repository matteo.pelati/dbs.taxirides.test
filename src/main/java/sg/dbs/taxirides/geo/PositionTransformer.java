package sg.dbs.taxirides.geo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.gavaghan.geodesy.GlobalCoordinates;


public interface PositionTransformer {

    @AllArgsConstructor @Data @NoArgsConstructor
    class Location {
        double x;
        double y;
    }

    Location transform(GlobalCoordinates coords) throws IllegalArgumentException;
}
