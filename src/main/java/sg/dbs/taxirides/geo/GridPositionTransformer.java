package sg.dbs.taxirides.geo;

import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GlobalCoordinates;
import static java.lang.Math.*;



public class GridPositionTransformer implements PositionTransformer {

    final GlobalCoordinates startPos;
    final double squareSizeMeters;
    final int squareCount;

    GeodeticCalculator geoCalc = new GeodeticCalculator();
    Ellipsoid reference = Ellipsoid.WGS84;

    public GridPositionTransformer(GlobalCoordinates pos, double squareSizeMeters, int squareCount) {

        double startLat = geoCalc.calculateEndingGlobalCoordinates(
                reference, pos, 0, squareSizeMeters/2
        ).getLatitude();
        double startLong = geoCalc.calculateEndingGlobalCoordinates(
                reference, pos, 270, squareSizeMeters/2
        ).getLongitude();
        this.startPos = new GlobalCoordinates(startLat, startLong);

        this.squareSizeMeters = squareSizeMeters;
        this.squareCount = squareCount;
    }

    double getBearing(GlobalCoordinates standPoint, GlobalCoordinates forePoint) {

        double y = sin(toRadians(forePoint.getLongitude() - standPoint.getLongitude())) * cos(toRadians(forePoint.getLatitude()));
        double x = cos(toRadians(standPoint.getLatitude())) * sin(toRadians(forePoint.getLatitude()))
                - sin(toRadians(standPoint.getLatitude())) * cos(toRadians(forePoint.getLatitude())) * cos(toRadians(forePoint.getLongitude() - standPoint.getLongitude()));
        double bearing = (atan2(y, x) + 2 * PI) % (2 * PI);
        return toDegrees(bearing);
    }

    @Override
    public Location transform(GlobalCoordinates p) throws IllegalArgumentException {

        double northSouthBearing = getBearing(startPos, new GlobalCoordinates(p.getLatitude(), startPos.getLongitude()));
        double eastWestBearing = getBearing(startPos, new GlobalCoordinates(startPos.getLatitude(), p.getLongitude()));

        if (northSouthBearing == 0)
            throw new IllegalArgumentException("North/West of bounding box");
        if (eastWestBearing > 180 && eastWestBearing < 360)
            throw new IllegalArgumentException("North/West of bounding box");

        double latOffset = geoCalc.calculateGeodeticCurve(
                reference, startPos,
                new GlobalCoordinates(p.getLatitude(), startPos.getLongitude())
        ).getEllipsoidalDistance();
        double y = Math.floor(latOffset / squareSizeMeters) + 1;

        double longOffset = geoCalc.calculateGeodeticCurve(
                reference, startPos,
                new GlobalCoordinates(startPos.getLatitude(), p.getLongitude())
        ).getEllipsoidalDistance();
        double x = Math.floor(longOffset / squareSizeMeters) + 1;

        if (x > squareCount || y > squareCount)
            throw new IllegalArgumentException("South East of bounding box");

        return new Location(x, y);
    }

}
