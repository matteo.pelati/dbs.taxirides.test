TaxiRides
============

The program uses Hazelcast Jet to create and run an execution pipeline.
Currently, Hazelcast Jet is configured to run locally, without
any parallel processing.

The pipeline is defined in the provided configuration file ```pipeline.yaml```

The pipeline is defined as a simple sequence of 6 processors:

- **A file-based streaming source**: This processor is provided out-of-the-box 
  from Hazelcast Jet and monitors a directory for new files. Whenever a file is
  added, the content is streamed to the pipeline.
- **A CSV decoder**: Each line piped into the pipeline is decoded by this processor 
 and transformed into a Bean representing a taxi ride.
- **A location converter**: This processor is responsible of mapping the coordinates 
    of the input object with the indexes of a custom grid.
- **A Time-based sliding window**: This processor mantains a sliding window of all the
trip occourred in a specified amount of time. Whenever trips are added or removed from
 the sliding window, the pipeline is notified about the event. This processor expects 
 monotonically increasing timestamped events. All incoming events not satisfying
 this requirement are silently discarted.
- **A leaderboard of the most popular source/destination pairs**: The events generated
 by the sliding window are processed by this processor in order to mantain a state
 of the most popular trip source/destination pairs. Whenever the list of the top X
 trip destinations change, the pipeline is notified
- **A logger**: This processor simply receives all messages form the leaderboard,
formats them and emits them to the console.

## Building it

To build the artifacts just run 

```
mvn clean compile package
```

## Running it

Launch the pipeline processor using the command

```
java -jar target/taxirides.jar pipeline.yaml
```

This command will instantiate the pipeline processor. When the message 
```Started to watch directory: .data``` will appear, the pipeline is ready
to process data.

In order to trigger the processing, just copy a csv file into the ```.data```
folder with the following command:

```
cp data.csv ./.data
```


